export const general = {
	init: function() {
		this.formcontact();
	},
	formcontact: function() {
		const form = document.getElementById("formcontact");
		const pristine = new Pristine(form);
	    formcontact.addEventListener("submit", function(event) {
			event.preventDefault();
			let valid = pristine.validate({});
			console.log(valid);
			if(valid) {
				form.submit();
			}
	    });
	}
}