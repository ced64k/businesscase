import { componentLoader } from "./helpers/components-loader";

function App() {
	componentLoader.page = document
	.querySelector(".wrapper")
	.getAttribute("data-page");
	componentLoader.checkInitializedPage();
}

App.prototype.init = function() {

};

const app = new App();
app.init();