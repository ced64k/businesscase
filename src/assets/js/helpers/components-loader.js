import { general } from "../components/general";

export const componentLoader = {
	page: null,
	checkInitializedPage: function() {
		this.enableDefaultComponents();
		switch (this.page) {
			case "home":
				break;
			default:
		}
	},
	enableDefaultComponents: function() {
		general.init();
	}
};