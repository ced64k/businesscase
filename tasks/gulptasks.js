const del = require("del");
const gulp = require("gulp");
const sass = require("gulp-sass");
const twig = require('gulp-twig');
const OUT_DIR = "./dist";

// always copy the html first to dist folder
const srcHTML = "./app/template/index.html";
const srcCSS = "./app/template/style.css";
const srcAssets = "./app/assets/*";
const sassGlob = require('gulp-sass-glob');
const svgSprite = require('gulp-svg-sprite');


var paths = {
    base: {
        src: './src',
        dest: './dist'
    },
    styles: {
        src: 'src/assets/scss/*.scss',
        dest: 'dist/assets/css'
    },
    images: {
        src: 'src/assets/img/*',
        dest: 'dist/assets/img'
    },
    scripts: {
        src: 'src/assets/js/scripts.js',
        dest: 'dist/assets/js'
    },
    vendor: {
        src: 'src/assets/js/vendor/*',
        dest: 'dist/assets/js/vendor'
    },
    html: {
        src: 'src/templates/*.twig',
        dest: 'dist'
    }
};


function clean(done) {
    // Clean the dist folder
    del.sync([paths.base.dest]);

    // Signal completion
    return done();
}

function copyHtml(done) {
    gulp.src(paths.html.src).pipe(gulp.dest(paths.html.dest));
    done();
}

function copyCss(done) {
    gulp.src(paths.styles.src).pipe(gulp.dest(paths.styles.dest));
    done();
}

function style() {
    return (
        gulp
            .src(paths.styles.src)
            .pipe(sassGlob())
            .pipe(sass())
            .on("error", sass.logError)
            .pipe(gulp.dest(paths.styles.dest))
    );
}

function templates() {
    return (
        gulp
            .src(paths.html.src)
            .pipe(twig({
                data: {
                    title: 'Gulp and Twig',
                    benefits: [
                        'Fast',
                        'Flexible',
                        'Secure'
                    ]
                }
            }))
            .pipe(gulp.dest(paths.html.dest))
    );
}

function images() {
    return (
        gulp
            .src(paths.images.src)
            .pipe(gulp.dest(paths.images.dest))
    );
}

function vendor() {
    return (
        gulp
            .src(paths.vendor.src)
            .pipe(gulp.dest(paths.vendor.dest))
    );
}

exports.clean = clean;
exports.copyHtml = copyHtml;
exports.copyCss = copyCss;
exports.style = style;
exports.templates = templates;
exports.images = images;
exports.vendor = vendor;
