const path = require("path");
const outputDir = path.resolve(__dirname, "dist/assets/js");

module.exports = {
    mode: "development",
    entry: path.resolve(__dirname, "src/assets/js/scripts.js"),
    devtool: "inline-source-map",
    output: {
        path: outputDir,
        filename: "scripts.js"
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ["babel-loader"]
            },
            {
                test: /\.js$/,
                use: ["source-map-loader"],
                enforce: "pre"
            }
        ]
    }
};
